import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'champs-year-selector',
  templateUrl: './year-selector.component.html',
  styleUrls: ['./year-selector.component.css']
})
export class YearSelectorComponent implements OnInit {

  private yearsList: Array<Object> = [];
  
  constructor(public apiService:ApiService) { }

  ngOnInit() {
    this.createYearsList();
  }

  private createYearsList():void {
    for(let i=5; i <= 17; i++) {
      this.yearsList.push({ year: 2000 + i});
    }
  }

  private onSelectYear(year:number):void {
   this.apiService.setYear(year);
  }

}
