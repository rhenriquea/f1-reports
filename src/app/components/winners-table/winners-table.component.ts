import { Component, AfterContentInit } from '@angular/core';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'champs-winners-table',
  templateUrl: './winners-table.component.html',
  styleUrls: ['./winners-table.component.css']
})
export class WinnersTableComponent implements AfterContentInit {

  private racesList: Array<any> = [];
  private driversList: Array<any> = [];
  private winnersList: Array<any> = [];
  private tableLoaded: Boolean = false;
  private hasError: Boolean = false;

  constructor(private apiService: ApiService) { }

  ngAfterContentInit() {
    this.loadTableData(2005);

    this.apiService.getYear().subscribe((year) => {
      this.tableLoaded = false;
      this.driversList = [];
      this.winnersList = [];
      this.loadTableData(year);
    });
  }

  private loadTableData(year: number) {
    this.apiService.getChampionsListByYear(year)
      .subscribe(res => {
        this.tableLoaded = true;
        this.racesList = res.MRData.RaceTable.Races;
        this.getDriversList();
      }, (error)=>{
        this.tableLoaded = false;
        this.hasError = true;
        console.log(error)
      });
  }

  private checkVictories(driverCode) {
    return this.winnersList.indexOf(driverCode) > -1;
  }

  private getDriversList() {
    this.racesList.forEach(race => {
      let driverCode = race.Results[0].Driver.code;
      let isOnList = this.driversList.indexOf(driverCode) > -1;
      !isOnList ? this.driversList.push(driverCode) : this.winnersList.push(driverCode);
    });
  }
  
}
