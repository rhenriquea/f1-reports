import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { ListComponent } from './pages/list/list.component';
import { SummaryComponent } from './pages/summary/summary.component';

const routes: Routes = [   
    { path: 'home', component: HomeComponent},
    { path: 'list', component: ListComponent},
    { path: 'summary', component: SummaryComponent},
    { path: '**', redirectTo: 'home' } 
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutesModule {
    constructor() { }
}
