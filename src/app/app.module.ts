import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutesModule } from './app-routes.module';
import { ChartsModule } from 'ng2-charts';

import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { WinnersTableComponent } from './components/winners-table/winners-table.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { YearSelectorComponent } from './components/year-selector/year-selector.component';

import { HomeComponent } from './pages/home/home.component';
import { ListComponent } from './pages/list/list.component';
import { SummaryComponent } from './pages/summary/summary.component';

import { ApiService } from './services/api.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    SpinnerComponent,
    YearSelectorComponent,
    WinnersTableComponent,
    ListComponent,
    SummaryComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule,
    FormsModule,
    HttpModule,
    AppRoutesModule,
    ChartsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})

export class AppModule { }

