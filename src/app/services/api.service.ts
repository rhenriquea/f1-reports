import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Subject } from 'rxjs';

import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {
    public selectedYear = new Subject<Number>();
    
    constructor(private http: Http) { }
    
    public getChampionsListByYear(year:number) {
        return this.http.get(`http://ergast.com/api/f1/${year}/results/1.json`)
        .map((res) => res.json());
    }

    public setYear(year: number){
        this.selectedYear.next(year);
    }

    public getYear(): Observable<any> {
        return this.selectedYear.asObservable();
    }

}