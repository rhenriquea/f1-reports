import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'champs-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {

  private racesList: Array<any> = [];
  private driversDetails: Array<any> = [];
  private driversDetailsTable: Array<any> = [];
  private chartLoaded = false;
  private tableLoaded = false;
  // Chart
  public chartLabels: string[] = [];
  public chartData: number[] = [];
  public chartType: string = 'pie';
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.loadTableData(2005);

    this.apiService.getYear().subscribe((year) => {
      this.driversDetails = [];
      this.driversDetailsTable = [];
      this.chartLoaded = false;
      this.tableLoaded = false;
      this.loadTableData(year);
    });
  }

  private loadTableData(year: number) {
    this.apiService.getChampionsListByYear(year)
      .subscribe(res => {
        this.racesList = res.MRData.RaceTable.Races;
        this.getDriversDetails();
      }, (error) => {
      });
  }

  getDriversDetails() {
    this.racesList.forEach(race => {
      this.driversDetails.push(race.Results[0].Driver);
    });
    this.getFinalResults();
  }

  private getFinalResults() {
    
    let drivers = this.driversDetails.reduce((driversCount, currentDriver) => {
      driversCount[`${currentDriver.givenName} ${currentDriver.familyName}`] = (driversCount[`${currentDriver.givenName} ${currentDriver.familyName}`] || 0 ) + 1;
      return driversCount;
    },{});
    
    var driverLabels = [];
    var chartData = [];

    for(var x in drivers){
      driverLabels.push(x);
      chartData.push(drivers[x]);
      this.driversDetailsTable.push({driver:x, winning: drivers[x]});
    }

    this.chartData = chartData;
    this.chartLabels = driverLabels;
    this.chartLoaded = true;
    this.tableLoaded = true;
  }

}
