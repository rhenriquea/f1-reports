import { F1ReportsPage } from './app.po';

describe('f1-reports App', () => {
  let page: F1ReportsPage;

  beforeEach(() => {
    page = new F1ReportsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
