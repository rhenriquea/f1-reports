# F1 Reports

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.0.

## Starting the project

1. Clone the repository in your local machine.

2. Inside the folder where you clonated the repository, run `npm install` to install all the projects dependencies.

3. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Projects Dependencies

This project was built using:
[Ng2 Charts](https://valor-software.com/ng2-charts/), 
[Ergast Developer API](http://ergast.com/mrd/) and
[Ngx Bootstrap](https://valor-software.com/ngx-bootstrap/#/) 
